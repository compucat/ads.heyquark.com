---
---

exports.handler = async function(event, context) {
    const ads = [
    {% for ad in site.ads %}
        "{{ site.url }}{{ ad.url | escape }}",
    {% endfor %}
    ];
    const ad = ads[Math.floor(Math.random() * ads.length)];
    return {
        statusCode: 302,
        headers: { "Location": encodeURI(ad) },
    }
}
